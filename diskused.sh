echo "**************************************"
echo "Verifying the used space on the server"
echo "**************************************"
echo "%s %s\n"
printf "%s %s\n" "$(date)" "$line"
df -a
echo "**************************************"
echo "Verification completed successfully"
echo "**************************************"
